#!/usr/bin/env python

import random

class weapon:
    def __init__(self,name,attacks,damage):
        self.name=name
        self.attacks=tuple(attacks)
        self.damage=int(damage)

    def __str__(self):
        return self.name

class monster:
    def __init__(self,name,attacks,hitpoints):
        self.name=name
        self.attacks=tuple(attacks)
        self.hitpoints=int(hitpoints)

    def __str__(self):
        return self.name

# List of places, with connections.  Make geographical changes or 
# additions here.
# Mapping of (place,action) into (place,consequence)
Places={('cave','north'):('other_cave',['see']),
        ('other_cave','south'):('cave',['see']),
        ('other_cave','east'):('serpents_chamber',['see']),
        ('serpents_chamber','west'):('other_cave',['see']),
        ('serpents_chamber','up'):('palace',['see']),
        ('palace','down'):('serpents_chamber',['see']),
        ('palace','north'):('outer_wall',['see']),
        ('palace','south'):('riches_room',['see']),
        ('outer_wall','qwer'):('valley',['see']),
        ('outer_wall','south'):('palace',['see']),
        ('valley','west'):('road',['see']),
        ('valley','north'):('road',['see']),
        ('road','east'):('valley',['see']),
        ('road','south'):('valley',['see'])}

# Enumeration Of Places.  Each Should Have A Name, A List Of Monsters, 
# And An Inventory Of Stuff That'S Initially Found In The Place.



other_cave={'description':"""You're in a large chamber made of white rock.\n
                             there's a passage to the south,
                             and a narrow tunnel to the east.
                          """,
            'inventory':[]}


cave={'description':"""You're in a cave of jagged black stone.\n
                       There's a passage to the north.
                    """,
      'inventory':[weapon('stick',['thwacks','bonks','pokes'],1)],
      'monsters':[monster('mouse',['squeaks','nibbles','scurries'],0)]}

serpents_chamber={'monsters':[monster("serpent",["strikes"],6)],
                  'description':"""You are in the hall of serpents.\n
                             There's a tunnel to the west and stairs going up.
                                """,
                  'inventory':[weapon('sword',['slashes','cuts','stabs'],7),
                               'baby']}

palace={'monsters':[monster('phoenix',['sings'],4)],
        'description':"""You're in a huge palace.\n
                         There is a path to the north, south and a stairwell going down.
                      """,
        'inventory':['gold']}



outer_wall={'description':"""You are facing a huge wall.\n 
                             The gate is barred.
                          """,
            'inventory':['piece of paper that says "magic word qwer"']}


riches_room={'description':"""You are standing on a pile of gold.\n
                              there is a passage to the north.
                           """,
             'inventory':['gold','gold','gold']}


valley={'description':"""You are standing in a small valley.\n
                         There are trails going to the north, west and south.
                      """,
        'inventory':[]}

road={'description':"""You are on a road curving back toward the valley.\n
                       The path exits to the south and east.
                    """,
      'inventory':[weapon('trident',['stab','strike'],5)],
      'monsters':[]}


grand_cavern={'monsters':[monster('ogre',['slams','bashes'],7),
                          monster('ogress',['gnashes','bams','bites'],8)],
              'description':"""You are in a cave of glowing crystal.""",
              'inventory':[]}




##### Think twice before making changes below here.

def Universe(place):
    return eval(place)

# List of actions that can always be taken, regardless of place.
generic_actions=['take','drop','inventory','attack','look']

def solicit_action(place,state):
    inputstr=raw_input('What action do you take? ')
    words=inputstr.strip().lower().split(" ")
    if len(words)>1:
        thing=words[1]
    else:
        thing=''
    verb=words[0]
    if words[0] in ['walk','go','run','crawl']:
        verb=thing
        thing=''
    elif verb in ['look','gaze','examine']:
        verb='look'
    elif verb in ['take','grab','acquire','steal','possess','snatch','pick','up']:
        verb='take'
    elif verb in ['drop','discard','lose','abandon']:
        verb='drop'
    elif verb in ['attack','destroy','hit','kill','decapitate']:
        verb='attack'
    elif verb in ['inventory','inv','items']:
        verb='inventory'
        thing=''

    if verb in generic_actions:
        return verb,thing
    try:
        Places[(place,verb)]
        return verb,thing
    except KeyError:
        print "That's not an option.  Try again."
        return solicit_action(place,state)

def implement_consequence(place,consequence,thing,state):
    if consequence=='see':
        print Universe(place)['description']
        try:
            for critter in Universe(place)['monsters']:
                print "There is a %s here." % critter
        except KeyError:
            pass
        try:
            for item in Universe(place)['inventory']:
                print "There is %s here." % item
        except KeyError:
            pass # No inventory?
    elif consequence=='gain':
        try:
            stuff=Universe(place)['inventory']
            mystuff=state['inventory']
            mystuff+=[stuff.pop([str(x) for x in stuff].index(thing))]
        except ValueError:
            print "You can't take that!"
    elif consequence=='lose':
        try:
            mystuff=state['inventory']
            stuff=Universe(place)['inventory']
            stuff+=[mystuff.pop([str(x) for x in mystuff].index(str(thing)))]
        except ValueError:
            print "You don't have that!"
    elif consequence=='inventory':
        print "You have:"
        for item in state['inventory']: print " - %s" % item
    elif consequence=='attack':
        state,thing=combat_round(state,thing)
        if thing.hitpoints<0:
            critters=Universe(place)['monsters']
            critters.pop([str(x) for x in critters].index(str(thing)))
            print "The %s dies." % thing
        else: 
            print "The %s has %d hitpoints." % (thing.name,thing.hitpoints)
    elif consequence=='die':
        state['alive']=False
    elif consequence=='attacked':
        try:
            print "The %s %s!" % thing
        except KeyError:
            print "There are no monsters here."

    return state


def changes(place,action,thing='',state=None):
    if len(thing)>0: # Deal with generic actions that act on things here
        if action=='take':
            state=implement_consequence(place,'gain',thing,state)
            print "Okay."
            return place,[]
        elif action=='drop':
            state=implement_consequence(place,'lose',thing,state)
            print "Okay."
            return place,[]
        elif action=='attack':
            try:
                idx=[str(m) for m in Universe(place)['monsters']].index(thing)
                thing=Universe(place)['monsters'][idx]
                state=implement_consequence(place,'attack',thing,state)
            except ValueError:
                print "That's not something that's here."
            return place,[]
        else:
            print "I don't know what you want to do with %s." % thing
    else: # Deal with generic actions that don't act on things here
        if action=='inventory':
            return place,['inventory']
        elif action=='look':
            return place,['see']
        try:
            return Places[(place,action)]
        except KeyError:
            print "That's not feasible."
            return place,[]

def combat_round(player,monster):
    """
    Round of combat.
    """
    # Attacks depend on weapon and player characteristic
    if player['wielding'] is None:
        using=weapon('teeth and nails',['claw','bite','scratch','gnaw'],1)
    else: using=player['wielding']
    
    print "You %s the %s with your %s." % (random.choice(using.attacks),monster.name,using.name)
    if random.random()>0.5:
        print "You hit!"
        monster.hitpoints+=-1
    else:
        print "You miss!"

    return (player,monster)
    

if __name__=='__main__':
    action='look'
    place='cave'
    state={'alive':True,
           'inventory':[],
           'hitpoints':8,
           'wielding':None}

    thing=''
    while state['alive']:
        place,consequences=changes(place,action,thing,state)
        for consequence in consequences:
            state=implement_consequence(place,consequence,thing,state)
        if state['alive']:
            action,thing=solicit_action(place,state)

    print "You are now dead.  Thanks for playing!"
